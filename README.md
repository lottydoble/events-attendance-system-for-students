EVENTS ATTENDANCE SYSTEM FOR STUDENTS

Doble, Charlotte Denise S.

Caballero, Christian Poochee


username: admin@mail.com
password: 123456.

Functionalities:
	
	1. Will serve as an eventual attendance checker for students by their ID.
	
	2. Upon monitor, informations such as First Name, Last Name, Address and Birthdate of the student.
	
	3. The events that are monitored can also be edited - the event's name, description and when it was held.
	
	4. This system generates the attendance from a list of participants for each event.

