<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendee extends Model
{
    public function events(){
    	return $this->belongsTo(Event::class, 'events');
    }

    public function participants(){
    	return $this->belongsTo(Participant::class, 'participants');
    }
}
