<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attendee;
use App\Participant;
use Session;

class AttendeeController extends Controller
{
    public function store(Request $request)
    {
        //
    	if (!Participant::where('id', $request->pid)->exists()) {
        	$msg = 'Participant existence is questioned.';
        	return redirect()->route('event.show', $request -> eid)->withMessage($msg);
    	}
    	if (Attendee::where('event_id', $request->eid)->where('participant_id', $request->pid)->exists()) {
        	$msg = 'Participant is already here.';
        	return redirect()->route('event.show', $request -> eid)->withMessage($msg);
    	}

        $attendee = New Attendee;

        $attendee->event_id = $request -> eid;
        $attendee->participant_id = $request -> pid;

        $attendee->save();

        return redirect()->route('event.show', $request -> eid);
    }
}
