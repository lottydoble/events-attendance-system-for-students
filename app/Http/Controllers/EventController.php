<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Attendee;

class EventController extends Controller
{
    public function index()
    {
        //
        $event = Event::paginate(5);

        return view('event.index')->withEvents($event);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $event = New Event;

        $event->name 		= $request -> name;
        $event->description = $request -> description;
        $event->eventdate 	= $request -> eventdate;

        $event->save();

        return redirect()->route('event.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $event = Event::find($id);

        $attendee = Attendee::leftJoin('participants', 'attendees.participant_id', '=', 'participants.id')->select('participants.firstname', 'participants.lastname', 'attendees.created_at')->where('event_id',$id)->paginate(10);

        return view('event.event')->withEvent($event)->withAttendees($attendee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	$event = Event::find($id);

        $event->id          = $request->id;
        $event->name    	= $request->name;
        $event->description = $request->description;
        $event->eventdate   = $request->eventdate;

        $event->save();

        return redirect()
            ->route('event.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
