<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Participant;

class ParticipantController extends Controller
{
    public function index()
    {
        //
        $participant = Participant::paginate(10);

        return view('participant.index')->withParticipants($participant);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('participant.participant');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $participant = New Participant;

        $participant->firstname = $request -> firstname;
        $participant->lastname = $request -> lastname;
        $participant->address = $request -> address;
        $participant->birthdate = $request -> birthdate;

        $participant->save();

        return redirect()->route('participant.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	
        $participant = Participant::find($request->input('id'));

        $participant->id           = $request->id;
        $participant->firstname    = $request->firstname;
        $participant->lastname     = $request->lastname;
        $participant->address      = $request->address;
        $participant->birthdate    = $request->birthdate;

        $participant->save();

        return redirect()
            ->route('participant.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
