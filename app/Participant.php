<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participant extends Model
{
    public function attendees(){
    	return $this->hasMany(Attendee::class, 'attendees');
    }
}
