<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
    }
}

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//Insert into Users Table
		DB::table('users')->insert([
            'name' => 'Super Administrator',
            'email' => 'admin@mail.com',
            'password' => bcrypt('123456.'),
        ]);
    }
}
