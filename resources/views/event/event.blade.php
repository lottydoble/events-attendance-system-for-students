@extends('layouts.layouts')

@section('content')

	<div class="container">
		<h2>{{ $event->name }}</h2>
		<h4>{{ $event->description }}</h4>
		<h3>List of Participants</h3>

		@if(Session::has('message'))
			<div class="container alert alert-success alert-dismissable">
				{{Session::get('message')}}
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		  			<span aria-hidden="true">&times;</span>
				</button>
			</div>
		@endif
		@auth
			@if($event->eventdate == date("Y-m-d"))
			<div>
				{!! Form::open(['route' => 'attendee.store', 'Method' => 'POST']) !!}
			    {!! Form::hidden('eid', $event->id, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}
				{!! Form::label('Participant ID') !!}
				{!! Form::text('pid', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}
				{!! form::submit('Check In', ['class' => 'btn btn-success'])!!}
				{!! Form::close() !!}
			</div>
			@endif
		@endauth

		<table class="table table-hover">
			<thead>
				<tr>
					<td>Firstname</td>
					<td>Lastname</td>
					<td>Timein</td>
				</tr>
			</thead>
			<tbody>
				@foreach($attendees as $attendee)
					<tr>
						<td>{{ $attendee->firstname }}</td>
						<td>{{ $attendee->lastname }}</td>
						<td>{{ $attendee->created_at }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<center>{{ $attendees->links() }}</center>
	</div>
@endsection

