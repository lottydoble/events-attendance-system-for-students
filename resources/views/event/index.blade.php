@extends('layouts.layouts')

@section('content')

	<div class="container">
		<h3>List of events</h3>
		<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal1">
            Add event
        </button>

		<table class="table table-hover">
			<thead>
				<tr>
					<td>Event Name</td>
					<td>Description</td>
					<td>Date</td>
				</tr>
			</thead>
			<tbody>
				@foreach($events as $event)
					<tr>
						<td>{{ $event->name }}</td>
						<td>{{ $event->description }}</td>
						<td>{{ $event->eventdate }}</td>
						<td>
							@auth
							<div class="btn-group pull-right">
                          		<button class="edit-modal btn btn-success" data-toggle="modal" data-target="#editevent" data-id="{{$event->id}}" data-name="{{$event->name}}" data-description="{{$event->description}}" data-eventdate="{{$event->eventdate}}">
                            		<span class="glyphicon glyphicon-pencil"></span>
                          		</button>
                  			</div>
                  			@endauth
						</td>
						<td>
                  			<div>
                          		<a class="btn btn-success" href={{ route('event.show', $event->id) }}>
                            		<span class="glyphicon glyphicon-eye-open"></span>
                          		</a>
                  			</div>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<center>{{ $events->links() }}</center>
	</div>
@endsection


@section('modals')
@parent
<div id="editevent" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="panel panel-primary">
          	<div class="panel-heading">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <h4 class="panel-title" id="myModalLabel"><b>Edit Event</b></h4>
          	</div>
	        <div class="modal-body">
	        
	        	@if(isset($event->id))
	            {!! Form::model($event, ['route' => ['event.update', $event->id], 'method' => 'PUT']) !!}
	            {!! Form::hidden('id', null, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}

				{!! Form::label('Event Name') !!}
				{!! Form::text('name', null, ['id' => 'ename', 'class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

				{!! Form::label('Description') !!}
				{!! Form::text('description', null, ['id' => 'edesc', 'class' => 'form-control', 'required' => '', 'maxlength' => '256']) !!}

				{!! Form::label('Date') !!}
				{!! Form::date('eventdate', null, ['id' => 'edate', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

	            <div class="modal-footer">
	                <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
	                {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
	            </div>
	            	{!! Form::close() !!}
	            	@endif
	        </div>
      	</div>
    </div>
</div>    

{{-- Add User Modal --}}
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		  	<div class="modal-content">
			    <div class="modal-header">
			    	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    	<h4 class="modal-title" id="myModalLabel">Add event</h4>
			    </div>
		      	<div class="modal-body">
			        {!! Form::open(['route' => 'event.store', 'Method' => 'POST']) !!}

					{!! Form::label('Event Name') !!}
					{!! Form::text('name', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

					{!! Form::label('Description') !!}
					{!! Form::text('description', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

					{!! Form::label('Event Date') !!}
					{!! Form::date('eventdate', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}
				</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			        {!! form::submit('Save', ['class' => 'btn btn-success'])!!}
		      	</div>
		      	{!! Form::close() !!}
		    </div>
		</div>
	</div>
@endsection

@section('scripts')
@parent
    {{-- Edit User Modal --}}
    <script type="text/javascript">
        $(document).on('click', '.edit-modal', function() {
            $('#uid').val($(this).data('id'));
            $('#ename').val($(this).data('name'));
            $('#edesc').val($(this).data('description'));
            $('#edate').val($(this).data('eventdate'));

            $('#editevent').modal('show');
        });
    </script>
@endsection