@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card card-default">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <a class="btn btn-default" href={{ route('participant.index') }} role="button">Show Participants</a>
                    <a class="btn btn-default" href={{ route('event.index') }} role="button">Show Events</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
