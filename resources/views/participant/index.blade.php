@extends('layouts.layouts')

@section('content')

	<div class="container">
		<h3>List of Participants</h3>
		<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal1">
            Add Participant
        </button>

		<table class="table table-hover">
			<thead>
				<tr>
					<td>ID</td>
					<td>Firstname</td>
					<td>Lastname</td>
					<td>Address</td>
					<td>Birthdate</td>
				</tr>
			</thead>
			<tbody>
				@foreach($participants as $participant)
					<tr>
						<td>{{ $participant->id }}</td>
						<td>{{ $participant->firstname }}</td>
						<td>{{ $participant->lastname }}</td>
						<td>{{ $participant->address }}</td>
						<td>{{ $participant->birthdate }}</td>
						<td>
							<div class="btn-group pull-right">
                          		<button class="edit-modal btn btn-success" data-toggle="modal" data-target="#editparticipant" data-id="{{$participant->id}}" data-lastname="{{$participant->lastname}}" data-firstname="{{$participant->firstname}}" data-address="{{$participant->address}}" data-birthdate="{{$participant->birthdate}}">
                            	<span class="glyphicon glyphicon-pencil"></span>
                          		</button>
                  			</div>
					</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<center>{{ $participants->links() }}</center>
	</div>
@endsection


@section('modals')
@parent
<div id="editparticipant" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="panel panel-primary">
          	<div class="panel-heading">
	            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <h4 class="panel-title" id="myModalLabel"><b>Edit participant</b></h4>
          	</div>
	        <div class="modal-body">
	        
	        	@if(isset($participant->id))
	            {!! Form::model($participant, ['route' => ['participant.update', $participant->id], 'method' => 'PUT']) !!}
	            {!! Form::hidden('id', null, ['id' => 'uid', 'class' => 'form-control', 'required' => '']) !!}

				{!! Form::label('Firstname') !!}
				{!! Form::text('firstname', null, ['id' => 'fname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

	            {!! Form::label('Lastname') !!}
				{!! Form::text('lastname', null, ['id' => 'lname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

				{!! Form::label('Address') !!}
				{!! Form::text('address', null, ['id' => 'uaddress', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

				{!! Form::label('Birthdate') !!}
				{!! Form::date('birthdate', null, ['id' => 'ubirthdate', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

	            <div class="modal-footer">
	                <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
	                {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
	            </div>
	            	{!! Form::close() !!}
	            	@endif
	        </div>
      	</div>
    </div>
</div>    

{{-- Add User Modal --}}
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
		  	<div class="modal-content">
			    <div class="modal-header">
			    	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    	<h4 class="modal-title" id="myModalLabel">Add participant</h4>
			    </div>
		      	<div class="modal-body">
			        {!! Form::open(['route' => 'participant.store', 'Method' => 'POST']) !!}

					{!! Form::label('Firstname') !!}
					{!! Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

					{!! Form::label('Lastname') !!}
					{!! Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

					{!! Form::label('Address') !!}
					{!! Form::text('address', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

					{!! Form::label('Birthdate') !!}
					{!! Form::date('birthdate', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}
				</div>
		      	<div class="modal-footer">
			        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
			        {!! form::submit('Save', ['class' => 'btn btn-success'])!!}
		      	</div>
		      	{!! Form::close() !!}
		    </div>
		</div>
	</div>
@endsection

@section('scripts')
@parent
    {{-- Edit User Modal --}}
    <script type="text/javascript">
        $(document).on('click', '.edit-modal', function() {
            $('#uid').val($(this).data('id'));
            $('#lname').val($(this).data('lastname'));
            $('#fname').val($(this).data('firstname'));
            $('#uaddress').val($(this).data('address'));
            $('#ubirthdate').val($(this).data('birthdate'));

            $('#editparticipant').modal('show');
        });
    </script>
@endsection